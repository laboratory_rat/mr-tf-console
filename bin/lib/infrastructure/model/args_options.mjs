
/**
 * options class for train tasks
 *
 * @export
 * @class TrainOptions
 */
export class TrainOptions {

  /**
   * Creates an instance of TrainOptions.
   * @param {*} args
   * @memberof TrainOptions
   */
  constructor(args) {
    args = args || {};
    this.configFile = args.configFile;
    this.datasetFile = args.datasetFile;
    this.modelDirectory = args.modelDirectory;
    this.hiddenNeurons = args.hiddenNeurons || [50];
    // this.lstmCells = args.lstmCells || 2;
    this.frameSize = args.frameSize || 50;
    this.feature = args.feature;
    this.epochs = args.epochs || 5;
    this.learningRate = args.learningRate || 0.01;
    this.validationSplit = args.validationSplit || 0.3;
    this.useBias = args.useBias || false;
    this.activation = args.activation || 'linear';
  }
}

export class ValidationOptions {
  constructor(args) {
    args = args || {};
    this.configFile = args.configFile || null;
    this.datasetFile = args.datasetFile || null;
    this.modelDirectory = args.modelDirectory || null;
    this.feature = args.feature || null;
    this.frameSize = args.frameSize || 50;
    this.outputFile = args.outputFile;
    this.outputLabel = args.outputLabel;
  }
}


export class PredictOptions {
  constructor(args) {
    args = args || {};
    this.configFile = args.configFile || null;
    this.datasetFile = args.datasetFile || null;
    this.modelDirectory = args.modelDirectory || null;
    this.feature = args.feature || null;
    this.frameSize = args.frameSize || 50;
    this.predictionDeep = args.predictionDeep || null;
    this.outputFile = args.outputFile;
    this.outputLabel = args.outputLabel;
    this.fromStart = args.fromStart || false;
  }
}