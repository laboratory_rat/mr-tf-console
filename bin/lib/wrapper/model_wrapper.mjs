// import tf from '@tensorflow/tfjs-node';
import tf from '@tensorflow/tfjs-node-gpu';

/**
 * 
 * @param inputSize 
 * @param hiddenNeuronsCount 
 * @param epochs 
 * @param batchSize 
 * @param learningRate 
 * @param dataset 
 */
export async function createModel(inputSize, hiddenNeuronsCount, epochs, validationSplit, learningRate, useBias, activation, dataset) {
  let model = tf.sequential();
  const inputLayerShape = [inputSize];
  const rnn_input_layer_features = 10;
  const rnn_input_layer_timestamps = inputSize / rnn_input_layer_features;
  const rnn_input_shape = [rnn_input_layer_features, rnn_input_layer_timestamps];

  model.add(tf.layers.dense({
    units: inputSize,
    inputShape: inputLayerShape
  }));

  model.add(tf.layers.reshape({
    targetShape: rnn_input_shape
  }));

  let lstmCells = [];
  for (let index = 0; index < hiddenNeuronsCount.length; index++) {
    lstmCells.push(tf.layers.lstmCell({
      units: hiddenNeuronsCount[index],
      activation: activation,
      useBias: useBias
    }));
  }

  model.add(tf.layers.rnn({
    cell: lstmCells,
    inputShape: rnn_input_shape,
    returnSequences: false,
  }));

  model.add(tf.layers.dense({
    units: 1,
    inputShape: [hiddenNeuronsCount[hiddenNeuronsCount.length - 1]],
  }));

  model.compile({
    optimizer: tf.train.adam(learningRate),
    loss: 'meanSquaredError'
  });

  let X = dataset.map(x => x[0]);
  let Y = dataset.map(x => x[1]);
  const xs = tf.tensor2d(X, [X.length, X[0].length]).div(tf.scalar(10));
  const ys = tf.tensor2d(Y, [Y.length, 1]).reshape([Y.length, 1]).div(tf.scalar(10));

  const hist = await model.fit(xs, ys, {
    batchSize: inputSize,
    epochs: epochs,
    validationSplit: validationSplit > 1 || validationSplit < 0 ? null : validationSplit
  });

  return {
    model: model,
    stats: hist
  };
}

export function calculateModelPrediction(model, features) {
  return model.predict(tf.tensor2d(features));
}

export async function saveNModel(model, path) {
  await model.save('file://' + path);
}

export async function loadNModel(path) {
  return await tf.loadLayersModel(`file://${path}/model.json`);
}