export function transformToMAvarage(data, frame) {
  const result = [];
  console.log(frame);
  for (var step = 0; step + frame < data.length; step++) {
    result.push(data.slice(step, step + frame).reduce((sub, a) => sub += a) / frame);
  }

  return result;
}