import yaml from 'js-yaml';
import fs from 'fs';

export function writeObjectToYaml(data, path) {
  if (fs.existsSync(path)) {
    fs.rmdirSync(path, {
      recursive: true
    });
  }
  fs.writeFileSync(path, yaml.safeDump(data), 'utf8');
}

export function readYamlToObject(path) {
  let fileContent = fs.readFileSync(path);
  return yaml.safeLoad(fileContent);
} 