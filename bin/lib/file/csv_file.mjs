import fs from 'fs';
import csvParser from 'csv-parser';
import csvWriter from 'csv-writer';

export async function readCsv(filePath) {
  if (!fs.existsSync(filePath)) {
    throw `File ${filePath} do not exists`;
  }

  const results = [];
  return new Promise((resolve) => {
    fs.createReadStream(filePath)
      .pipe(csvParser())
      .on('data', (line) => results.push(line))
      .on('end', () => resolve(results));
  });
}

export async function writeCsv(filePath, data) {
  if (fs.existsSync(filePath)) {
    fs.rmdirSync(filePath, {
      recursive: true
    });
  }

  const labels = Object.keys(data[0]).map(x => ({id: x, title: x}));
  const writer = csvWriter.createObjectCsvWriter({
    header: labels,
    path: filePath
  });

  await writer.writeRecords(data);
}