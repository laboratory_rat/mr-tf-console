import { TrainOptions, ValidationOptions, PredictOptions } from './lib/infrastructure/model/args_options.mjs';
import { readCsv, writeCsv } from './lib/file/csv_file.mjs';
import { createModel, saveNModel, loadNModel } from './lib/wrapper/model_wrapper.mjs';
import { writeObjectToYaml } from './lib/file/yaml_file.mjs';
import commander from 'commander';
import { readYamlToObject } from './lib/file/yaml_file.mjs';
import fs from 'fs';
import { calculateModelPrediction } from './lib/wrapper/model_wrapper.mjs';
const { program } = commander;

async function bootstrap() {
  const trainProgramm = new commander.Command()
    .name('train-lstm')
    .description('create new time seria prediction model')
    .storeOptionsAsProperties(false)
    .passCommandToAction(false)
    .option('-y, --config-file <path>', 'config file with arguments')
    .option('-d, --dataset-file <path>', 'dataset to process', './data/input.csv')
    .option('-m, --model-directory <path>', 'directory to save trained model', './models/output')
    .option('-h, --hidden-neurons <numbers...>', 'hidden neurons count', [50])
    .option('-a, --activation <value>', 'activation function')
    // .option('-c, --lstm-cells <number>', 'LSTM cells count', 2)
    .option('-s, --frame-size <number>', 'how many example take at one time', 50)
    .option('-f, --feature <value>', 'feature to analyze')
    .option('-e, --epochs <number>', 'epochs count', 10)
    .option('-r, --learning-rate <number>', 'learing rate', 0.01)
    .option('-b, --use-bias', 'use bias for hidden layers', false)
    .option('-v, --validation-split <number>', 'split data for better validation (from 0 to 1)')
    .action(runTrain);

  const validationProgramm = new commander.Command()
    .name('validate-lstm')
    .description('validate model with well known data')
    .storeOptionsAsProperties(false)
    .passCommandToAction(false)
    .option('-y, --config-file <path>', 'yaml config file')
    .option('-d, --dataset-file <path>', 'dataset to process')
    .option('-m, --model-directory <path>', 'target model directory')
    .option('-f, --feature <value>', 'feature column title')
    .option('-s, --frame-size <number>', 'frame to proccess')
    .option('-o, --output-file <path>', 'file where store output results')
    .option('-l, --output-label <value>', 'label of predicted resulst')
    .action(runValidation);

  const predictProgramm = new commander.Command()
    .name('predict-lstm')
    .description('predict values, based on already know seria')
    .storeOptionsAsProperties(false)
    .passCommandToAction(false)
    .option('-y, --config-file <path>', 'yaml config file')
    .option('-d, --dataset-file <path>', 'dataset file')
    .option('-m, --model-directory <path>', 'directory with trained model')
    .option('-f, --feature <value>', 'feature to grab from dataset file')
    .option('-s, --frame-size <number>', 'frame to process')
    .option('-p, --prediction-deep <number>', 'prediction length')
    .option('-o, --output-file <path>', 'path of the output file')
    .option('-l, --output-label <value>', 'label of predicted results')
    .option('-t, --from-start', 'start prediction from first readed part')
    .action(runPredict);

  program
    .name('mr-bonobo')
    .version('1.0.0')
    .addCommand(trainProgramm)
    .addCommand(validationProgramm)
    .addCommand(predictProgramm);

  await program.parseAsync(process.argv);
}

async function runTrain(_rawOptions) {
  let options = new TrainOptions(_rawOptions);
  if (options.configFile) {
    const uploadedOptions = readYamlToObject(options.configFile);
    options = {
      ...options,
      ...uploadedOptions
    };
  }

  if (!options.modelDirectory) throw 'Model directory is required';
  if (!options.datasetFile || !fs.existsSync(options.datasetFile)) throw 'Dataset is required';
  if (fs.existsSync(options.modelDirectory)) {
    fs.rmdirSync(options.modelDirectory, {
      recursive: true
    });
  }

  const featuresList = (await readCsv(options.datasetFile))
    .map(x => Number(x[options.feature]));
  const dataset = createDataset(featuresList, options.frameSize);
  let trainResult = await createModel(
    options.frameSize,
    options.hiddenNeurons,
    options.epochs,
    options.validationSplit,
    options.learningRate,
    options.useBias,
    options.activation,
    dataset);
  const { model, stats } = trainResult;
  await saveNModel(model, options.modelDirectory);
  writeObjectToYaml({
    options,
    stats
  }, `${options.modelDirectory}/meta.yaml`);
}

async function runValidation(_rawOptions) {
  let options = new ValidationOptions(_rawOptions);
  if (options.configFile) {
    const uploadedOptions = readYamlToObject(options.configFile);
    options = {
      ...options,
      ...uploadedOptions
    };
  }

  const model = await loadNModel(options.modelDirectory);
  const fullCsv = (await readCsv(options.datasetFile))
    .map(x => ({
      ...x,
      [options.outputLabel]: 0
    }));
  const rawDataset = fullCsv.map(x => Number(x[options.feature]));
  const dataset = [];
  for (let step = 0; step + options.frameSize < rawDataset.length; step++) {
    dataset.push(rawDataset.slice(step, step + options.frameSize));
  }

  dataset.forEach(async (entity, index) => {
    let targetCsvItem = fullCsv[index + options.frameSize];
    targetCsvItem[options.outputLabel] = calculateModelPrediction(model, [entity]).dataSync()[0];
    console.log(
      `finished: ${index + 1} / ${fullCsv.length - options.frameSize}`,
      ((index + 1) / (fullCsv.length - options.frameSize) * 100).toFixed(2) + '%');
  });

  await writeCsv(options.outputFile, fullCsv);
}

async function runPredict(_rawOptions) {
  let options = new PredictOptions(_rawOptions);
  if (options.configFile) {
    const uploadedOptions = readYamlToObject(options.configFile);
    options = {
      ...options,
      ...uploadedOptions
    };
  }

  const model = await loadNModel(options.modelDirectory);
  let fullCsv = (await readCsv(options.datasetFile));

  const seedDataset = (options.fromStart
    ? fullCsv.slice(0, options.frameSize)
    : fullCsv.slice(fullCsv.length - options.frameSize - 1, fullCsv.length - 1))
    .map(x => Number(x[options.feature]));
  const results = [];

  for (let step = 0; step < options.predictionDeep; step++) {
    let prediction = calculateModelPrediction(model, [seedDataset]).dataSync()[0];
    console.log(
      `step: ${step + 1} / ${options.predictionDeep}`,
      `${((step + 1) / (options.predictionDeep) * 100).toFixed(2)}%`,
      `prediction: ${prediction}`
    );
    results.push(prediction);
    seedDataset.shift();
    seedDataset.push(prediction);
  }

  if (options.feature != options.outputLabel) {
    fullCsv = fullCsv.map(x => ({
      ...x,
      [options.outputLabel]: 0
    }));
  }

  results.forEach((result, index) => {
    let csvIndex = options.fromStart
      ? index + options.frameSize
      : fullCsv.length + index;
    let entity = csvIndex >= fullCsv.length
      ? { [options.outputLabel]: result }
      : {
        ...fullCsv[csvIndex],
        [options.outputLabel]: result
      };
    fullCsv[csvIndex] = entity;
  });

  await writeCsv(options.outputFile, fullCsv);
}

function createDataset(list, frameSize) {
  const result = [];
  for (let step = 0; step + frameSize < list.length; step++) {
    result.push([list.slice(step, step + frameSize), list[step + frameSize]]);
  }
  return result;
}

bootstrap()
  .then(() => console.log('Program finished'))
  .catch((_) => console.error('Critical error', _));