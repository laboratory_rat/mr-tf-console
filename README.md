# Required soft
 - [nodejs](https://nodejs.org/en/) (better use >= 14)
 - [cuda drivers](https://developer.nvidia.com/cuda-10.0-download-archive) only 10.0 version!
 - [cudnn SDK](https://developer.nvidia.com/rdp/cudnn-download) for 10.0 version

##
[npm program](https://www.npmjs.com/package/mr-bonobo)